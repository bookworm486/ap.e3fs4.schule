---
tags:
- FISI
- FIAE
---

# Kapitalbedarfsplan

Der Kapitalbedarfsplan zur Unternehmensgründung ermittelt den Betrag an finanziellen Mitteln, der benötigt wird, um mit dem Unternehmen zu starten und die erste Aufbauphase zu überstehen.

Die genaue Ermittlung des Kapitalbedarfs gehört zu den wichtigsten Aufgaben jedes Unternehmensgründers.
Eine exakte Kapitalbedarfsplanung hilft, ständige Zahlungsbereitschaft zu sichern und Zahlungsunfähigkeit zu vermeiden.
Es gilt, nicht nur den langfristigen Kapitalbedarf für das Anlagevermögen wie Grundstücke, Gebäude, Maschinen und Fahrzeuge zu erfassen, sondern auch den kurzfristigen Kapitalbedarf für das Umlaufvermögen, beispielsweise für das Material- und Warenlager sowie Roh-, Hilfs- und Betriebsstoffe.

Auch die Gründungskosten, die Kosten für die Anlaufphase, den Kapitaldienst, d.h. die Zahlung von Zins und Tilgung für ein eventuell aufgenommenes Existenzgründungsdarlehen.

[Checkliste Kapitalbedarfsplan](./kapitalbedarfsplan_checkliste.pdf)