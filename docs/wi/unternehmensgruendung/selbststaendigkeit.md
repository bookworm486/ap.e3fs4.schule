---
tags:
- FISI
- FIAE
---

# Berufliche Selbsständigkeit

Ausgangspunkt der Überlegungen, sich selbstständig zu machen, sollte eine pfiffige Produkt- oder Geschäftsidee sein.
Der Unternehmensgründungsprozess kann von der Idee bis zur Verwirklichung mehrere Wochen oder gar Monate dauern.

## 1. Geschäftsidee entwickeln

Zu Beginn jeder Unternehmensgründung steht eine Geschäftsidee.
Sie knüpft häufig an die bisherige berufliche Fähigkeit bzw. an das Hobby des Unternehmensgründers an.
Im besten Fall handelt es sich dabei um eine Lösung eines weitverbreiteten Problems, die Entwicklung einer neuen Technologie oder um neuartige Möglichkeiten vorhandene Technologien zu verwenden.

## 2. Sachliche Voraussetzungen klären

Bei der Prüfung der sachlichen Voraussetzungen hat der Existenzgründer insbesondere folgende Punkte zu klären: 

1. Marktchancen und Marktpositionierung  
  Der Unternehmensgründer muss überprüfen, ob für seine Geschäftsidee ein entsprechender Bedarf vorhanden ist.
  Dazu muss er die gegenwärtig vorherrschenden Marktgegebenheiten auf seinem Zielmarkt untersuchen z.B. Anzahl potenzieller Kunden und deren Vermögensverhältnisse, Beschaffung von Daten konkurrierender Unternehmen.  
  Die Marktpositionierung legt fest, welche Rolle ein Unternehmen auf dem Markt einnehmen möchte:

    | Marktführer | Nischenbesetzer |
    | ----------- | --------------- |
    | Hat i.d.R. den größten Anteil am relevanten Markt | Kleinere Unternehmen |
    | Ist führend bei Preisänderungen, innovativen Produkten, Werbung | Beschränken sich auf Teilmärkte |
    | Die Konkurrenz orientiert sich an ihm | Spezialisieren sich auf Marktnischen (Marktlücken), die von größeren Unternehmen vernachlässigt werden. |

2. Standort  
  Der Standort ist für den Erfolg eines Unternehmens mit entscheidend.
  Man muss einen Standort finden, der die Zielsetzung des Unternehmens unterstützt und ist z.B. von Bedeutung für

    * Lieferanten- und Kundennähe
    * Möglichkeit qualifizierte Mitarbeiter zu bekommen
    * Die Höhe der Kosten (Miete, Pacht, Lohnkosten usw.)
    * Umfang der Umweltauflagen
    * Güte der Verkehrsanbindung

3. Personalbedarf  
  Die Anzahl der Mitarbeiter (quantitativer Personalbedarf) muss einerseits so groß sein, dass alle Aufgaben erfüllt werden können.
  Andererseits ist darauf zu achten, dass keine Personalüberbesetzung besteht.
  Neben der Anzahl der Mitarbeiter hat der Unternehmensgründer auf deren Qualifikation zu achten (qualitativer Personalbedarf).
  Sie muss möglichst deckungsgleich mit dem Anforderungsprofil der zu besetzenden Stelle sein.
4. Gesicherte Finanzierung  
  Im Falle einer Neugründung, aber auch bei der Verwirklichung einer neuen Produkt- oder Geschäftsidee in einem bestehenden Unternehmen sind zunächst erhebliche Finanzmittel für die Betriebs- und Geschäftsausstattung (z.B. Maschinen, Büroeinrichtung, Kasse) und die Beschaffung der Waren (z.B. Rohstoffe, Handelswaren) sowie anfallende Gründungskosten notwendig, bevor Geld verdient wird. I.d.R. werden Bankkredite aufgenommen oder Investoren gesucht.