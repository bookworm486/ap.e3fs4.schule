---
tags:
- FISI
- FIAE
---

# Konjunktur

Die Konjunktur gibt Auskunft über die wirtschaftliche Lage eines Landes.
Die wirtschaftliche Lage eines Landes verläuft in Wellenbewegungen, die als Konjunkturschwankungen bezeichnet werden.
Ein idealtypischer Konjunkturzyklus sieht folgendermaßen aus:

![Typischer Konjunkturverlauf](../img/wi/Konjunkturzyklus.jpeg)

## Konjunkturphasen

![Phasen des Konjunkturverlaufes](../img/wi/Konjunkturzyklus.jpg)

## Konjunkturindikatoren

Konjunkturindikatoren sind Daten, die den Konjunkturverlauf messen und/oder Voraussagen (Prognosen) für künftige Entwicklungen zulassen.
Nachfolgend werden wichtige Konjunkturindikatoren genannt und deren Auswirkungen beschrieben.

| Konjunkturindikator | Mögliche Auswirkungen auf die Konjunktur |
| ------------------- | ---------------------------------------- |
| Entwicklung der Arbeitslosenquote und offene Stellen | Eine steigende Arbeitslosenquote und sinkende offene Stellen zeigen an, dass die Wirtschaft unterbeschäftigt ist. Die Unternehmen werden sich bei den Investitionen zurückhalten, weil sie eine stagnierende oder zurückgehende Konsumgüternachfrage erwarten. Bei einer steigenden Zahl offener Stelen bzw. wenn die Arbeitslosenquote zurückgeht tritt die umgekehrte Reaktion ein. |
| Aufragseingänge | Steigende Auftragsbestände kündigen einen Konjunkturaufschwung an. Rückläufige Auftragsbestände kündigen einen Konjunkturabschwung an. |
| Bruttoinlandsprodukt | Ist das BIP gegenüber der Vorperiode gestiegen (gefallen), zeigt dies eine Konjunkturbelebung (einen Konjunkturrückgang an) |

## Konjunkturpolitik

Die Konjunkturschwankungen erklären sich dadurch, dass die gesamtwirtschaftliche Nachfrage im Vergleich zum gesamtwirtschaftlichen Angebot mal zu gering und mal zu hoch ist.
Indem auf die wichtigsten Bestimmungsgrößen eingewirkt wird, sollen die zyklischen Schwankungen möglichst verschwinden.
Konjunkturpolitik umfasst die wirtschaftlichen Maßnahmen, die darauf ausgerichtet sind, die Konjunktur zu glätten und ein möglichst gleichmäßiges Wirtschaftswachstum zu erreichen.
Die Nachfrageorientierte Konjunkturpolitik beruht auf der Annahme, dass allein der Staat die Konjunktur positiv beeinflussen kann.
Mit der Erhöhung oder Senkung der Staatsausgaben, handelt der Staat entgegen des Konjunkturzyklus - man spricht von antizyklischer Fiskalpolitik.
Die Fiskalpolitik kann die Nachfrager in zweierlei Hinsicht beeinflussen:

### Direkt

* Ausweitung von Staatsausgaben während einer Rezession -> Preisauftrieb verstärkt sich, aufgrund einer höheren Geldmenge.  
  Beispiele:  
    * Der Staat lässt Straßen und Brücken bauen. Damit erhöht sich die Beschäftigung in der Bauindustrie. Diese wiederum kann mehr Baumaterialien, mehr Maschinen mehr Kraftfahrzeuge und mehr Arbeitskräfte nachfragen
    * Einführung einer Abwrackprämie z.B. für Autos
    * Erhöhung von Subventionen
* Kürzung von Staatsausgaben während eines Aufschwungs -> Preisauftrieb wird gedämpft  
  Beispiele:
    * Verminderung der Staatsaufträge
    * Kürzung oder Streichung von Subventionen
    * Einstellungs- und Beförderungsstopp im öffentlichen Dienst

### Indirekt

* Senkung der Einkommensteuer während einer Rezession  
  Ziel: Belebung des privaten Konsums und der privaten Investitionen. Die nachfragewirksame Geldmenge wird höher, der Preisauftrieb verstärkt sich.
* Erhöhung der Einkommensteuer während eines Aufschwungs  
  Ziel: Drosselung des privaten Konsums und der privaten Investitionen. Die nachfragewirksame Geldmenge wird geringer, der Preisauftrieb wird gedämpft.

Die staatliche Konjunkturbeeinflussung ist in der Praxis häufig problematisch.
So soll der Staat z.B. während des Konjunkturabschwungs zusätzliche Ausgaben vornehmen und Steuern senken; andererseits sind gerade in dieser Phase die Staatseinnahmen durch zurückgehende Steuereinnahmen ohnehin schon gering.
Nicht zuletzt beeinflussen die Zukunftserwartungen sowohl in den Unternehmen als auch in den Haushalten das wirtschaftliche Handeln.

