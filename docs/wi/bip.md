---
tags:
- FISI
- FIAE
---

# Bruttoinlandsprodukt

Die wirtschaftliche Leistung einer Volkswirtschaft und deren Veränderung (Wirtschaftswachstum) werden von Statistischen Ämtern gemessen und als Bruttoinlandsprodukt (BIP) ausgewiesen.
Das BIP ist der Wert aller Waren und Dienstleistungen, die in einem bestimmten Zeitraum (meist ein Jahr) im Inland produziert werden.
Die Vorleistungen bleiben dabei unberücksichtigt.

Das BIP wird auf drei verschiedene Arten ermittelt.
Man kann errechnen, wo es entstand, wie es verwendet wurde und wie es verteilt wurde.

1. **Entstehungsrechnung**  
  Die Entstehungsrechnung gibt darüber Auskunft, in welchem Umfang die einzelnen Wirtschaftsbereiche in einer Periode zum Bruttoinlandsprodukt beigetragen haben.
2. **Verwendungsrechnung**  
  Die Verwendungsrechnung gibt darüber Auskunft wofür die Güter verwendet wurden.
  Es wird untersucht, ob die Waren und Dienstleistungen z.B. als Konsumausgaben, als Investitionen in Unternehmen oder im Außenbeitrag (Export-Import) Verwendung gefunden haben:
  <table>
    <thead>
        <tr>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Verwendung des Bruttoinlandsprodukts in der Bundesrepublik Deutschland 2017 (in Mrd. EUR)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Private Konsumausgaben</td>
            <td style="border:1px solid #b3adad; padding:5px;">1732,2</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Konsumausgaben des Staates</td>
            <td style="border:1px solid #b3adad; padding:5px;">638,9</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Bruttoinvestitionen</td>
            <td style="border:1px solid #b3adad; padding:5px;">658,5</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Außenbeitrag</td>
            <td style="border:1px solid #b3adad; padding:5px;">247,8</td>
        </tr>
        <tr style="background:#e5b8b7">
            <td style="border:1px solid #b3adad; padding:5px;">BIP 2017</td>
            <td style="border:1px solid #b3adad; padding:5px;">3277,3</td>
        </tr>
    </tbody>
  </table>

3. **Verteilungsrechnung**  
  Hier wird für eine Periode Auskunft darüber gegeben, wer das Geld für die erzeugten Güter und Dienstleistungen eingenommen hat, also ob es an die Arbeitnehmer ausbezahlt wurde oder ob es als Gewinne und Zinsen den Unternehmern zugeflossen ist.
  Ein Teil davon muss für Ersatzinvestitionen (Abschreibungen) verwendet werden.
  Dies sind Aufwendungen, um jene Güter zu ersetzen, die wegen der Produktion abgenutzt wurden.
  Die Summe aus Arbeitnehmerentgelt und dem Unternehmer- und Vermögenseinkommen bezeichnet man als **Volkseinkommen**.  
  Den prozentualen Anteil des Arbeitnehmerentgelts am Volkseinkommen bezeichnet man als Lohnquote.
  Den prozentualen Anteil des Unternehmens- und Vermögenseinkommens am Volkseinkommen bezeichnet man als Gewinnquote.
  <table>
    <thead>
        <tr>
            <th colspan="2" style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Verteilung des Volkseinkommens in der 
Bundesrepublik Deutschland 2017 (in Mrd. EUR)</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Arbeitnehmerentgelt</td>
            <td style="border:1px solid #b3adad; padding:5px;">1668,8</td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;">Unternehmens- und Vermögenseinkommen</td>
            <td style="border:1px solid #b3adad; padding:5px;">787,6</td>
        </tr>
        <tr style="background:#e5b8b7">
            <td style="border:1px solid #b3adad; padding:5px;">Volkseinkommen</td>
            <td style="border:1px solid #b3adad; padding:5px;">2456,4</td>
        </tr>
    </tbody>
  </table>

## Reales und Nominales BIP

Ein steigendes Bruttoinlandsprodukt bedeutet noch nicht, dass die Volkswirtschaft tatsächlich im angezeigten Umfang mehr produziert hat.
Das Wachstum kann vielmehr ganz oder teilweise an gestiegenen Preisen liegen.
Das zu jeweiligen Preisen bewertete BIP bezeichnet man als nominales BIP.
Will man die tatsächliche Mehrleistung einer Volkswirtschaft im Vergleich zum Vorjahr ermitteln, muss man das reale (preisbereinigte) BIP berechnen.
Dies geschieht im Kern dadurch, dass man aus dem nominalen BIP die Preisveränderungen herausrechnet.

**Kritik:**

* Das BIP wird teilweise zu hoch angesetzt:   
  Kosten, die nicht berücksichtigt werden sollten, werden in das BIP eingerechnet z.B. Behandlungskosten von Unfallopfern, Reparaturen.
* Das BIP wird teilweise zu niedrig angesetzt:  
  Vorgänge der Schattenwirtschaft (z.B. Schwarzarbeit) werden nur als Schätzgröße berücksichtigt.
  Eigenleistungen und ehrenamtliche Leistungen werden nicht hinzugerechnet z.B. selbst renoviertes Zimmer, Einsätze der freiwilligen Feuerwehr
* Das BIP erfasst nur quantitative Größen:  
  Nicht erfasst werden qualitative Größen z.B. Lebensqualität oder Qualität der medizinischen Versorgung.


## Weiterführende Links

### Videos

* [Bruttoinlandsprodukt | Pelzig](https://www.youtube.com/watch?v=p0SExf2EpqU)
* [Bruttoinlandsprodukt | einfach erklärt](https://www.youtube.com/watch?v=07anlgd5se0)
* [Reales und nominales BIP | einfach erklärt](https://www.youtube.com/watch?v=p83WzqLwmp8)
* [Bruttoinlandsprodukt | simpleclub](https://www.youtube.com/watch?v=2y5o7XlDXok)
* [Reales vs. nominales BIP | simpleclub](https://www.youtube.com/watch?v=XZDtTs1LjcU)