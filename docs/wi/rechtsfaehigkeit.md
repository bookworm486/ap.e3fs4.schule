# Rechtsfähigkeit

!!! note "Definition"

    Die Rechtsfähigkeit ist die Fähigkeit ein Träger von Rechten und Pflichten zu sein. Alle natürlichen und juristischen Personen sind rechtsfähig.

Menschen werden im Gesetz auch als Natürliche Personen bezeichnet und sind nach §1 BGB rechtsfähig.

Vereine (e.V.), Unternehmen (AG), Körperschaften (Städte, Gemeinden, …)  werden im Gesetz auch als Juristische Person bezeichnet und sind rechtsfähig.  
Es werden 2 Arten der Juristischen Personen unterschieden:
1. Juristische Person des Privaten Rechts:
    * eingetragene Verein (e.V.)
    * Stiftungen
    * Aktiengesellschaft (AG)
    * Gesellschaft mit beschränkter Haftung (GmbH)
2. Juristische Person des Öffentlichen Rechts:
    * Städte, Bund, Bundesländer, Gemeinden
    * Handwerkskammer
    * Öffentlich Rechtliche Rundfunkanstalten

![Rechtfähigkeit Übersicht](../img/wi/rechtsfaehigkeit_uebersicht.png)
