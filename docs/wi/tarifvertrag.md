# Tarifvertrag

!!! note "Definition"

    Ein Tarifvertrag ist ein Vertrag, in dem einheitliche Arbeitsbedingungen für die Arbeitnehmer ganzer Wirtschaftszweige einer Region festgelegt werden.  
    Tarifverträge werden von den **Tarifpartnern** abgeschlossen.
    Dies sind auf der Arbeitnehmerseite die **Gewerkschaften** und auf der Unternehmerseite die **Arbeitgeberverbände**.
    Die Vertragsüarteien werden auch **Sozialpartner** genannt.

## Übersicht

![Mindmap zu Tarifverträgen](../img/wi/tarifvertag_uebersicht.png)

## Tarifverhandlungen

![Tarifverhandlungen](../img/wi/tarifverhandlungen.png)