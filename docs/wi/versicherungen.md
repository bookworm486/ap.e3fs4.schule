# Versicherungen

## Sozialversicherungen

<table>
    <thead>
        <tr>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Versicherung</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Zweck</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Träger</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Leistungen</th>
            <th style="border:1px solid #b3adad; padding:5px; text-align:center; vertical-align: middle;">Beitragssatz</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Kranken-versicherung</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Versicherung bei Krankheit</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Ortskrankenkassen</li>
                    <li>Betriebskrankenkassen</li>
                    <li>Ersatzkassen</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Krankengeld</li>
                    <li>Behandlung im Krankenhaus</li>
                    <li>ärztliche Behandlung</li>
                    <li>Mutterschaftshilfe</li>
                    <li>Vorsorgeuntersuchungen</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                14,6% + Zusatzbeitrag 1,6% im Durchschnitt (7,3% + 50% vom Zusatzbeitrag zahlt der Arbeitnehmer; 7,3% + 50% vom Zusatzbeitrag zahlt der Arbeitgeber)
            </td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Renten-versicherung</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Vorsorge bei Erwerbsunfähigkeit</li>
                    <li>Vorsorge im Alter</li>
                    <li>Vorsorge für Hinterbliebene</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Deutsche Rentenversicherung</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Rente wegen Erwerbsunfähigkeit</li>
                    <li>Altersrente</li>
                    <li>Witwen- / Waisenrente</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                18,6% (9,3% zahlt Arbeitnehmer, 9,3% zahlt Arbeitgeber)
            </td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Arbeitlosen-versicherung</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Hilfe bei Arbeitslosigkeit</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Bundesagentur für Arbeit</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Vermittlung von Arbeitsplätzen</li>
                    <li>Berufsberatung</li>
                    <li>Arbeitslosengeld</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                2,6% (1,3% zahlt Arbeitnehmer, 1,3% zahlt Arbeitgeber)
            </td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Unfall-versicherung</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Unterstützung bei Arbeitsunfall</li>
                    <li>Unterstützung bei Berufskrankheit</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Berufsgenossenschaftemn</li>
                    <li>Unfallkassen</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Krankenhausbehandlung</li>
                    <li>Rehabilitation</li>
                    <li>Verletztem / Hinterbliebenenrente</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                Die Kosten tragen die Arbeitgeber
            </td>
        </tr>
        <tr>
            <td style="border:1px solid #b3adad; padding:5px;"><b>Pflege-versicherung</b></td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Absicherung bei ambulanter und stationärer Pflegebedürftigkeit</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>Krankenkasse</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                <ul>
                    <li>häusliche Pflege durch ambulanten Pflegedienst</li>
                    <li>Pflegegeld für private Pflegekraft</li>
                    <li>teil- und vollstationäre Pflege</li>
                </ul>
            </td>
            <td style="border:1px solid #b3adad; padding:5px;">
                3,05% (Arbeitgeber und Arbeitnehmer übernehmen jeweils einen Anteil von 1,525%; Kinderlose ab 23 Jahren zahlen einen Beitragszuschlag von 0,35%)
            </td>
        </tr>
    </tbody>
</table>

## Private Zusatzversicherungen

### Krankenversicherung

* Azubis müssen sich gesetzlich krankenversichern
* bei Wahl auf Leistungsunterschiede achten

### Private Haftpflichversicherung

* springt ein, wenn man anderen Schaden zufügt
* Azubis sind in ihrer ersten Ausbildung i. d. R. über die Haftpflichversicherung der Eltern mitversichert (meist bis 25 Jahre)

### Private Unfallversicherung

* greift bei unfallbedingter Arbeitsunfähigkeit
* ist weltweit und rund um die Uhr gültig
* Gesetzliche Unfallversicherung greift automatisch, wenn der Unfall am Arbeitsplatz oder auf dem Weg dorthin passiert

### Berufsunfähigkeitsversicherung

* greift bei krankheits- oder berufsbedingter Arbeitsunfähigkeit
* je jünger der Versicherte, desto geringer die Beiträge
* Beiträge abhängig vom Berufszweig (z.B. zahlt Maurer-Azubi mehr als ein Bürokaufmann)

### Hausratsversicherung

* steht in der eigenen Wohnung hochwertige Ausstattungm, ist eine Hausratversicherung sinnvoll
* Azubis, die noch bei den Eltern wohnen oder nur vorübergehend ausziehen, bleiben mit ihrem Hausrat über den elterlichen Vertrag versichert (aber begrenzte Versicherungssumme)

### Kfz-Versicherung

* Haftpflichtversicherung ist gesetzlich vorgeschrieben und kommt für alle Schäden eines Unfallgegners auf
* (Teil-)Kaskoversicherung reguliert die Schäden am eigenen Auto, z.B. Diebstahl, Vandalismus; Vollkaskoversicherung zahlt bei einem selbstverschuldeten Unfall auch die Schäden am eigenen Auto