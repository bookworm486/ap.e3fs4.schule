---
tags:
- FISI
- FIAE
---

# Wirtschaft

## Lehrplan

* [Die Rolle des Mitarbeiters in der Arbeitswelt aktiv ausüben](./#die-rolle-des-mitarbeiters-in-der-arbeitswelt-aktiv-ausuben)
* [Als Konsument rechtliche Bestimmungen in Alltagssituationen anwenden](./#als-konsument-rechtliche-bestimmungen-in-alltagssituationen-anwenden)
* [Wirtschaftliches Handeln in der Sozialen Marktwirtschaft beurteilen ](./#wirtschaftliches-handeln-in-der-sozialen-marktwirtschaft-beurteilen)
* [Entscheidungen im Rahmen einer beruflichen Selbstständigkeit treffen](./#entscheidungen-im-rahmen-einer-beruflichen-selbststandigkeit-treffen)

### Die Rolle des Mitarbeiters in der Arbeitswelt aktiv ausüben

**Die Schülerinnen und Schüler verfügen über die Kompetenz, ihre Berufsausbildung und berufliche Tätigkeit unter Berücksichtigung wesentlicher Rechts- und Schutzvorschriften zu analysieren. Sie setzen sich mit dem Sozialversicherungssystem auseinander und führen eine Lohnabrechnung sowie eine Einkommenssteuererklärung durch.**

Die Schülerinnen und Schüler charakterisieren das Konzept der dualen Berufsausbildung (Lernorte, Beteiligte).
Anhand des Ausbildungsvertrages und der gesetzlichen Bestimmungen arbeiten sie die rechtlichen Voraussetzungen zur Begründung von Ausbildungsverhältnissen sowie die Inhalte des Ausbildungsvertrages heraus.
Sie leiten hieraus Rechte und Pflichten der an der Berufsausbildung beteiligten Personen ab.
In diesem Zusammenhang untersuchen sie Konfliktsituationen in der Ausbildung und entwickeln Lösungsmöglichkeiten.
Sie beschreiben die Möglichkeiten der Beendigung von Ausbildungsverhältnissen.

Die Schülerinnen und Schüler analysieren ihren betrieblichen Arbeitsplatz unter Beachtung von Schutzvorschriften (Jugendarbeitsschutz, Arbeitszeit, Urlaub, Mutterschutz, Elternzeit) und deren Überwachung.
Sie erläutern die Auswirkungen der Schutzbestimmungen für die Arbeitnehmerinnen und Arbeitnehmer.

Die Schülerinnen und Schüler erarbeiten auf der Grundlage der entsprechenden Rechtsvorschriften die Anbahnung (zulässige und nicht zulässige Fragen) und das Zustandekommen eines Arbeitsvertrages bezüglich Form und Inhalt.
Sie benennen die Rechte und Pflichten der Vertragsparteien und wenden sie situativ an.
Sie erläutern die Möglichkeiten der Beendigung von Arbeitsverhältnissen.
In diesem Zusammenhang vergleichen sie unbefristete und befristete Arbeitsverhältnisse und wenden die Vorschriften des allgemeinen und besonderen Kündigungsschutzes an.
Die Schülerinnen und Schüler prüfen ein qualifiziertes Arbeitszeugnis (Leistung, Führung).

Im Rahmen der betrieblichen Mitbestimmung setzen sich die Schülerinnen und Schüler mit den gesetzlichen Bestimmungen zur Errichtung eines Betriebsrats, dessen Aufgaben und Bedeutung auseinander.
Sie vergleichen die drei Stufen der Mitbestimmung des Betriebsrats und ordnen sie situativ zu.

Die Schülerinnen und Schüler unterscheiden Tarifvertragsarten (Entgelt-, Rahmenentgelt- und
Manteltarifvertrag).
Sie erörtern die Bedeutung von Tarifverträgen und deren Wirkung für Arbeitnehmer unter Berücksichtigung der Tarifautonomie und der Tarifbindung.
Sie stellen den Ablauf von Tarifverhandlungen und Arbeitskampf (Schlichtung, Streik, Aussperrung) dar.

Die Schülerinnen und Schüler beschreiben die Grundzüge der Sozialversicherung (Versicherungspflicht, Träger) und erläutern die grundlegenden gesetzlichen Leistungen des jeweiligen
Versicherungszweiges.
Sie analysieren die Grenzen der Sozialversicherung und begründen hieraus die Notwendigkeit privater Zusatzversicherungen (Berufsunfähigkeitsversicherung, private Altersvorsorge, Haftpflichtversicherung).

Sie führen Lohnabrechnungen (Brutto-, Nettolohn, Auszahlungsbetrag) auf Basis des Zeitlohns durch.
Sie füllen die nötigen Steuerformulare für eine einfache Einkommenssteuererklärung (nicht selbstständige Arbeit) aus und berücksichtigen dabei steuermindernde Faktoren (Werbungskosten, Sonderausgaben).

## Als Konsument rechtliche Bestimmungen in Alltagssituationen anwenden

**Die Schülerinnen und Schüler verfügen über die Kompetenz, Bestimmungen für Verbraucher exemplarisch anhand von Gesetzestexten zu beschreiben und auf Rechtsfälle des privaten Bereichs anzuwenden. Sie treffen situationsbezogene Entscheidungen im Rahmen des privaten Geldverkehrs und können Zusammenhänge von Einkommen und Konsum, Sparen und Verschuldung aufzeigen.**

Die Schüler und Schülerinnen erklären das Zustandekommen von ein- und zweiseitigen Rechtsgeschäften (Willenserklärung) im privaten Bereich. Hierbei erläutern sie die Rechts- und Geschäftsfähigkeit der Vertragspartner und begründen besondere Formvorschriften.
Sie unterscheiden anfechtbare und nichtige Rechtsgeschäfte.

Am Abschluss eines Kaufvertrages (Antrag, Annahme, Bindung an das Angebot) zeigen sie die Rechte und Pflichten der Vertragspartner auf. 
Sie unterscheiden Besitz und Eigentum (Eigentumsübertragung bei beweglichen Sachen, Eigentumsvorbehalt).
Sie analysieren alltägliche Rechtsgeschäfte von Verbrauchern und prüfen das Vorliegen von Kaufvertragsstörungen (Mangelhafte Lieferung, Zahlungsverzug).
Unter Berücksichtigung der rechtlichen Rahmenbedingungen erläutern sie die Rechte von Käufer und Verkäufer.
Sie stellen die Bedingungen der regelmäßigen Verjährung dar.

Die Schülerinnen und Schüler stellen verschiedene Möglichkeiten der Verbraucherberatung (Verbraucherschutzorganisationen, Publikationen) dar.
Sie wenden auf situationsbezogene Beispiele das Fernabsatzrecht an.
Sie erläutern die Bedeutung von Allgemeinen Geschäftsbedingungen und beschreiben in diesem Zusammenhang die gesetzlichen Regelungen (Überraschungsklauseln, Verbot der Verkürzung gesetzlicher Fristen zur Sachmängelhaftung).

Die Schülerinnen und Schüler vergleichen Konditionen von Girokonten verschiedener Kreditinstitute, unterscheiden Formen des Zahlungsverkehrs (Barzahlung, Überweisung, Bankkarte, Kreditkarte, elektronische Zahlungssysteme) und begründen situationsabhängig eine geeignete Zahlungsform.
Des Weiteren vergleichen die Schülerinnen und Schüler unterschiedliche Anlageformen (Termingeld, Aktienfonds) im Hinblick auf Liquidität, Rentabilität und Sicherheit.
Sie arbeiten die Voraussetzungen für Verbraucherdarlehen hinsichtlich Kreditwürdigkeit, Form, Inhalt und Sicherheiten (Sicherungsübereignung, Bürgschaft, Lohnabtretung) heraus.
Darauf aufbauend beurteilen sie die Gefahr der eigenen Überschuldung und entwickeln Perspektiven bei akuter Schuldensituation (Haushaltsplan, Schuldnerberatung, Verbraucherinsolvenz).

## Wirtschaftliches Handeln in der Sozialen Marktwirtschaft beurteilen

**Die Schülerinnen und Schüler verfügen über die Kompetenz, die Preisbildung in Abhängigkeit von der Marktform darzustellen und wirtschaftspolitisches Handeln in einer am Leitbild der Sozialen Marktwirtschaft orientierten Wirtschaftsordnung zu analysieren.**

Die Schülerinnen und Schüler kennzeichnen den Markt als Ort des Zusammentreffens von Angebot und Nachfrage sowie als Ort der Preisbildung.
Sie unterscheiden Märkte anhand der Anzahl der Marktteilnehmer (Polypol, Angebotsoligopol, Angebotsmonopol) und erläutern deren Verhalten.
An einem Beispiel ermitteln sie tabellarisch und grafisch Gesamtangebot, Gesamtnachfrage und den Gleichgewichtspreis bei einem Polypol auf dem vollkommenen Markt.
Sie stellen die Auswirkungen von Angebots- und Nachfrageänderungen auf den Gleichgewichtspreis und die Gleichgewichtsmenge dar.

Ausgehend von den Nachteilen der freien Marktpreisbildung beschreiben sie die Einflussmöglichkeiten des Staates auf die Marktpreisbildung.
Darauf aufbauend kennzeichnen sie das Wesen der Sozialen Marktwirtschaft und erklären anhand von aktuellen Beispielen die Wirkung der Instrumente der Sozialen Marktwirtschaft (Sozialpolitik, Einkommenspolitik, Wettbewerbspolitik, Umweltpolitik).

Sie beschreiben die Vorgehensweise zur Ermittlung des Preisindex für die Lebenshaltung sowie den Zusammenhang zwischen Inflationsrate und Kaufkraft und die Auswirkung auf den Reallohn.
Sie erklären die nichtmonetären Ursachen und die Folgen von Inflation und Deflation. Das Bruttoinlandsprodukt (reales und nominales BIP) definieren die Schülerinnen und Schüler als gesamtwirtschaftliche Messgröße und nehmen zu seiner Funktion als Wohlstandsindikator kritisch Stellung.

Die Schülerinnen und Schüler beschreiben einen idealtypischen Konjunkturverlauf und vergleichen diesen anhand von Indikatoren (Auftragseingänge, Bruttoinlandsprodukt, Arbeitslosenquote) mit der realen wirtschaftlichen Entwicklung.
Sie stellen exemplarisch Maßnahmen zur Beeinflussung der Konjunktur (Staatsnachfrage, Einkommenssteuer) dar.
Dabei problematisieren sie die Auswirkungen der jeweiligen Maßnahme auf die Konjunkturindikatoren und ihre eigene Lebenssituation.

## Entscheidungen im Rahmen einer beruflichen Selbstständigkeit treffen

**Die Schülerinnen und Schüler verfügen über die Kompetenz, sich mit individuellen, wirtschaftlichen und rechtlichen Aspekten einer Unternehmensgründung auseinanderzusetzen.**

Unter Berücksichtigung der besonderen Anforderungen an eine Unternehmerpersönlichkeit erörtern die Schülerinnen und Schüler private und berufliche Chancen und Risiken (soziale Sicherung, Verantwortung, Einkommen/Vermögen, Arbeitsbelastung) sowie Motive einer hauptberuflichen Selbstständigkeit.

Ausgehend von einer konkreten Geschäftsidee skizzieren die Schülerinnen und Schüler exemplarisch einen Geschäftsplan (Inhalt, Funktion, Adressaten).
Sie setzen sich mit der Bedeutung von Standortfaktoren im Kontext zur Geschäftsidee auseinander.
Sie vergleichen Rechtsformen (GbR, Einzelunternehmen, GmbH/UG) anhand verschiedener Merkmale (Mindestkapital, Haftung, Geschäftsführung) und prüfen, welche Rechtsform sich unter Berücksichtigung der persönlichen Voraussetzungen und der Geschäftsidee am besten eignet.

Die Schülerinnen und Schüler ermitteln auf der Grundlage der Geschäftsidee exemplarisch den Kapitalbedarf bei einer Unternehmensgründung (Kapitalbedarfsplan).