---
tags:
- FISI
- FIAE
---

# Marktwirtschaft

## Freie Marktwirtschaft

Als wesentliches Merkmal der freien Marktwirtschaft ist die **Freiheit des Einzelnen** zu nennen.
Jeder Einzelne ist somit für sich selbst verantwortlich und strebt, je nachdem ob es sich um einen Nachfrager oder Anbieter handelt, nach Nutzen- oder Gewinnmaximierung.
Dieses Modell, das in reiner Form nicht existiert, besagt:  
Wenn jeder seinen eigenen Vorteil sucht, dann wird der Unternehmer die vom Verbraucher gewünschten Waren herstellen und versuchen einen möglichst hohen Gewinn zu erzielen.
Der Verbraucher wird die Waren wiederum dort kaufen, wo der Preis am niedrigsten ist.
Daraus folgt, dass die egoistischen Motive der Anbieter zur Gewinnmaximierung dem Gesamtwohl dienen.
In der Theorie bringt die freie Marktwirtschaft die Einzel- und Gesamtinteressen zum Ausgleich.

### Wesentliche Merkmale

- **Konsumfreiheit:** Allein die Konsumenten entscheiden, was und wie viel gekauft wird.
- **Produktionsfreiheit:** Allein die Unternehmen entscheiden, was und wie viel produziert wird.
- **Freihandel:** Unternehmen und Haushalten ist überlassen, ob und wie viel sie importieren und exportieren wollen.
- **Vertragsfreiheit:** Vertragsgestaltung (z.B. bei Kaufverträgen) ist ausschließlich den Vertragspartnern überlassen.

Der Staat greift nicht in das Wirtschaftsgeschehen ein.
Er hat lediglich die Aufgabe, Schutz, Sicherheit und Eigentum der Bürger zu gewährleisten, ein Zahlungsmittel bereitzustellen sowie das Rechtssystem zu erhalten (wird auch als “Nachtwächterstaat“ bezeichnet).
Er überlässt die Steuerung der Wirtschaft allein dem Markt (dem Gesetz von Angebot und Nachfrage).

### Vor- und Nachteile

| Vorteile | Nachteile |
| -------- | --------- |
| Der Einzelne kann sich in Bezug auf seine Berufswahl, seine Vertragsfreiheit, seine Gewerbe- und Niederlassungsfreiheit vollständig entfalten | Die Startbedingungen und auch die Wettbewerbsvorteile können ungerecht sein |
| Es gibt keine wirtschaftlichen Einschränkungen, es wird also nur nach Leistung bezahlt und somit entsteht Wettbewerb | Löhne werden nur nach Angebot und Nachfrage bestimmt. Es gibt keine Mindestlöhne und keine Gewerkschaften |
| Jeder strebt für sich nach Gewinn und nach der Erfüllung der eigenen Ziele | Unternehmen können durch Monopole und Vormachtstellungen den Wettbewerb und dann den Preis kontrollieren |
|Hohe Wirtschaftlichkeit durch Leistungsdruck und dem Streben nach Gewinn | Es gibt keine soziale Verantwortung. Jeder ist nur für seinen Erfolg und seinen Gewinn verantwortlich |


## Soziale Marktwirtschaft

Die soziale Marktwirtschaft ermöglicht die Vorteile der freien Marktwirtschaft und versucht gleichzeitig ihre Nachteile zu vermeiden.
Im Grunde soll so viel Freiheit wie möglich und so viel staatlicher Zwang wie nötig gewährleistet werden.
„Vater“ dieser Wirtschaftsordnung ist der erste Wirtschaftsminister der Bundesrepublik Deutschland Ludwig Erhard.
Er verwendete diesen Begriff, als er nach 1948 die Marktwirtschaft in der Bundesrepublik einführte.

## Wesentliche Merkmale

* **Freiheit der Märkte:**
  Freiheit und Verantwortung gehen in der sozialen Marktwirtschaft Hand in Hand.
  Wie in der reinen Form der freien Marktwirtschaft regeln Angebot und Nachfrage den Wirtschaftsablauf.
  Um große soziale Ungerechtigkeiten zu vermeiden sorgt der Staat für einen sozialen Ausgleich (z.B. Lebensunterhalt in schwierigen Lebenssituationen)
* **Gewerbe-, Vertrags- und Konsumfreiheit:**
  Die Gewerbefreiheit ist eingeschränkt, um Verbraucher und die Allgemeinheit zu schützen.
  So ist u.a. für erlaubnispflichtige Gewerbe eine behördliche Zulassung erforderlich (z.B. Handel mit freien Arzneimitteln, Automatenaufstellung).
  Gefährliche Anlagen und bestimmte Gewerbezweige werden staatlich überwacht (Windkraftanlagen, Mineralölraffinerien).
  Umweltgesetze schränken die Gewerbefreiheit ein.
  Die Vertragsfreiheit wird durch gesetzliche Regelungen eingeschränkt, um z.B. sittenwidrige oder verbotene Geschäfte oder Ausbeutungen von Arbeitnehmern zu verhindern.
  Die Konsumfreiheit ist in manchen Branchen eingeengt.
  So dürfen bestimmte Arzneimittel von den Apotheken nur gegen ärztliches Rezept abgegeben werden.
* **Berufsfreiheit:**
  In der sozialen Marktwirtschaft besteht das Recht auf freie Wahl des Berufs, des Arbeitsplatzes und der Ausbildungsstätte.
  Diese Freiheit ist nicht unbegrenzt, da an manchen Berufen gewisse Voraussetzungen geknüpft sind z.B. Meisterprüfung für die Eröffnung eines Handwerksbetriebs oder staatlich Prüfungen für Beamte.
* **Eigentum:**
  Das Eigentumsrecht umfasst das Privateigentum an Konsumgütern, Produktionsmitteln sowie Grund und Boden.
  Der Staat hat jedoch Eingriffsrechte in das Privateigentum, wenn der Eingriff dem Wohl der Allgemeinheit dient.
* **Gleichheit vor dem Gesetz:**
  Gleicher Lohn für gleiche Arbeit d.h. also auch zwischen Mann und Frau oder zwischen In- und Ausländern und Chancengleichheit (z.B. BAföG, Umschulungen und Weiterbildungsmöglichkeiten, Ausbau von Schulen).
  Grundgesetz für die Bundesrepublik Deutschland
