---
tags:
- FISI
- FIAE
---

# Industrialisierung

## Entwicklung von Agrar- zur Informationsgesellschaft

|   | Erste Welle: | Zweite Welle: | Dritte Welle: | Vierte Welle: |
| - | ------------ | ------------- | ------------- | ------------- |
| **Merkmale:** | **Agrargesellschaft** | **Industriegesellschaft** | **Dienstleistungsgesellschaft** | **Informations-/Wissensgesellschaft** |
| **Wirtschaftssektoren** | Primär | Sekundär | Tertiär | Quartär |
| **Zeilicher Verlauf** | 18. + 19. Jhd. | 19. + 20. Jhd. | ab ca. 1970 | ab ca. 2000 |
| **Arbeitsteilung** | Gering | Hoch | Hoch | Hoch | 
| **Produktion** | Handwerk | Massenproduktion | Computer, Maschinen | Individualisierte Produktion, Roboter, Automatisierung |
| **Arbeitsverrichtung** | Handarbeit | Mehr Arbeit mit Maschinen | Erste elektronische Geräte | Beginn der Automatisierung |
| **Beschäftigung** | Familiär, Selbstständig | Arbeitszeiten, Arbeitgeber-/-nehmer Verhältnis | Feste Arbeitsverträge, besserer Arbeitsschutz, Karrierechancen | Flexible Arbeitszeiten (Homeoffice),-/Modelle | 
| **Familienbild** | Großfamilie | Kernfamilie | Kernfamilie | Patchwork Familie | 