---
tags:
- FISI
- FIAE
---

# Handlungskompetenz

!!! note "Definition"

    Handlungskompetenz ist die Fähigkeit, zielgerichtet, aufgabengemäß, der Situation angemessen und verantwortungsbewusst betriebliche Aufgaben zu erfüllen und Probleme zu lösen.

Die Handlungskompetenz lässt sich in folgende vier Bereiche einteilen:

## Sozialkompetenz

!!! note "Definition"

    Sozialkompetenz (SK) bezeichnet die Fähigkeit, gut mit anderen Menschen umgehen zu können.

### Anforderungen (für IT-Beruf)

* Durchsetzungsvermögen
* Teamfähigkeit
* Betreuung und Beratung
* Kooperationsbereitschaft

## Fachkompetenz

!!! note "Definition"

    Die Fachkompetenz (FK) umfasst alle für den jeweiligen Arbeitsbereich wichtigen Fähigkeiten und Fertigkeiten sowie das Fachwissen.

### Anforderungen (für IT-Beruf)

* Entscheidungsfreude
* Schulabschluss
* Interesse für IT
* Technisches Verständnis
* Betreuung von IT-Komponenten

## Personalkompetenz

!!! note "Definition"

    Die Personalkompetenz (PK) bezieht sich auf alle wünschenswerten Einstellungen und Verhaltensweisen einer Person.

### Anforderungen (für IT-Beruf)

* Einsatzfreudig
* Belastbarkeit
* Engagement
* Anpassungsfähig
* Weiterbildungsinteresse

## Methodenkompetenz

!!! note "Definition"

    Die Methodenkompetenz (MK) bezieht sich auf die Beherrschung von Lern- und Arbeitstechniken.

### Anforderungen (für IT-Beruf)

* Selbstständig
* Planvoll
* Systematisch
* Sorgfältig
* Informationsbeschaffung- und Auswertung

## Weiterführende Links

* [Wikipedia](https://de.wikipedia.org/wiki/Handlungskompetenz)
* [Handlungskompetenz - Landsiedel](https://www.landsiedel-seminare.de/coaching-welt/wissen/lexikon/handlungskompetenz.html)
