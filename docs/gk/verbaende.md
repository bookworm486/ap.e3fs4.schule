---
tags:
- FISI
- FIAE
---

# Verbände und Bürgerinitiativen

Interessenverbände wie der Deutsche Gewerkschaftsbund oder die Bundesvereinigung der Deutschen Arbeitgeberverbände bündeln die Interessen ihrer Mitglieder und vertreten diese gegnüber den staatlichen Entscheidungsträgern.
Dabei ist es ihr Ziel, die Gesetzgebung in ihrem Sinne zu beeinflussen, eigene Forderungen durchzusetzen oder den eigenen Interessen zuwiderlaufende Entscheidungen zu verhindern.
Andererseits sind die Verbände für eine erfolgreiche Politik unverzichtbar.
Ihre Vertreter sind wichtige Informationslieferanten, die bei der Vorbereitung politischer Entscheidungen ihren Sachverstand einbringen.  
Weitgehend in Konkurrenz zu Parteien und Verbänden haben sich seit den 1970er-Jahren die "neuen sozialen Bewegungen" gebildet. Sie entstehen als Reaktion auf Entwicklungen in Gesellschaft, Wirtschaft und Politik, die als problematisch oder als ungerecht empfunden werden.
Zu den neuen sozialen Bewegungen zählen Gruppen oder Organisationen, die in Größe und Aufbau ganz unterschiedlich sind:

* An einem Ende des Spektrums stehen Bürgerinitiativen, die als locker organisierte Zusammenschlüsse meist auf lokaler Ebene agieren, um über die Mobilisierung der Öffentlichkeit politische Entscheidungen in ihrem Sinne zu beeinflussen.
* Am anderen Ende des Spektrums finden sich die sogenannten Nichtregierungsorganisationen (NRO) wie Greenpeace oder Amnesty International, die sich als Antwort auf die Entstehung globaler Probleme oder zur Bekämpfung weltweiter Missstände zu übernational vernetzten Organisationen entwickelt haben.

Egal auf welcher Ebene die neuen sozialen Bewegungen aktiv sind, sie sind Ausdruck einer Bürgergesellschaft, die Politik aktiv mitgestalten will.

## Unterschied

Verbände sind Organisationen mit einer länger fristigen Zielsetzung auf ein oder wenige Themen. Bürgerinitiativen haben dagegen meist nur ein Thema und sind selten mittel oder langfristig organisiert.

## Weiterführende Links

* [Wikipedia - Verbände](https://de.wikipedia.org/wiki/Interessenverband)
* [Wikipedia - Bürgerinitiativen](https://de.wikipedia.org/wiki/B%C3%BCrgerinitiative)
* [Verband/Verbände - bpb](https://www.bpb.de/kurz-knapp/lexika/politiklexikon/18392/verband-verbaende/)
* [Interessenverbände - bpd](https://www.bpb.de/themen/politisches-system/deutsche-demokratie/39319/interessenverbaende/)
* [Bürgerinitiativen - bpd](https://www.bpb.de/kurz-knapp/lexika/handwoerterbuch-politisches-system/201988/buergerinitiativen/)

### Videos

* [Lobbyismus einfach erklärt - explainity ® Erklärvideos](https://www.youtube.com/watch?v=7xV0E38SMm0)
* [Was ist eine Bürgerinitiative? - lpbnrw ](https://www.youtube.com/shorts/VDhWLthvUt4)
* [Wie kann ich politisch mitwirken? - JUGEND PRÄGT](https://www.youtube.com/watch?v=BMz4dkwWnnk)

## Aktuell

* 