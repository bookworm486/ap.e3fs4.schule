---
tags:
- FISI
- FIAE
---

# Structured Query Language (SQL)

Sprache zur Kommunikation mit einer relationalen Datenbank.

## Befehle

---

### **Select**
Um Werte aus einer oder mehreren Tabellen zu bekommen.

__Beispiel:__
````sql
SELECT col_names FROM table_name;
````

### **Insert**
Um Daten in eine Tabelle zu schreiben.

__Beispiel:__
````sql
INSERT INTO table_name (col_names) VALUES (val_names);
````

### **Delete**
Um Daten aus einer Tabelle zu löschen.

__Beispiel:__
````sql
DELETE FROM table_name WHERE condition;
````

### **Update**
Um Datensätze in einer Tabelle zu updaten.

__Beispiel:__
````sql
UPDATE table_name SET col1 = val1, col2 = val2 WHERE condition;
````

### **Select Distinct**
Um Werte aus einer oder mehreren Tabellen zu bekommen, welche nicht den gleichen Inhalt haben.

__Beispiel:__
````sql
SELECT DISTINCT col_names FROM table_name;
````

## Bedingungen

---

### Where
Bedingung

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE condition;
````

### AND, OR, NOT
Mehrere Bedingungen, welche erfüllt werden müssen.  
Im folgenden Beispiel muss ein Datensatz die Kriterien erfüllen um abgerufen zu werden.
Entweder condition1 oder (condition 2 und nicht condition3).

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE condition1 OR condition2 AND NOT condition3;
````

### Is Null Bedingung
Überprüft, ob eine Spalte keinen Wert hat.
Kann verneint werden mit IS NOT NULL.

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE column_name IS NULL;
````

### Like Bedingung
Überprüft, ob der Wert einem Regex entspricht.
Kann verwendet werden, um string values zu überprüfen.
Oft werden folgende Operator mit LIKE verwendet.
"%": 0 oder mehrere Zeichen.
"_": Ein Zeichen.

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE column_name LIKE  pattern;

-- Alle Datensätze bei denen column_name mit a anfängt.
SELECT col_names FROM table_name WHERE column_name LIKE 'a%';
````

### Exists Bedingung
Testet, ob sich ein Datensatz in einer Sub-query befindet.

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE EXISTS(SELECT col_name FROM table_name);
````

### In Bedingung
Testet, ob ein Wert sich in einer Liste oder in den Werten von einer Sub-query befindet.

__Beispiel:__
````sql
SELECT col_names FROM table_name WHERE column_name IN (val1, val2, val3)

-- Testet ob der Wert von Country in der Liste ist.
SELECT * FROM Customers WHERE Country IN ('Germany', 'France', 'UK')

-- Testet ob der Wert in dem Rückgabewert von der Sub-query ist.
SELECT col_names FROM table_name WHERE EXISTS(SELECT col_name FROM table_name);
````

### Between Bedingung
Testet, ob ein Wert sich in einem Bereich befindet.

__Beispiel:__
````sql
SELECT column_names FROM table_name WHERE column_name BETWEEN 10 AND 20;
````

## Aggregatfunktionen

---

### MIN()
Finde den kleinsten Wert aus den Datensätzen.

__Beispiel:__
````sql
SELECT MIN(column_name) FROM table_name WHERE condition; 
````

### MAX()
Finde den größten Wert aus den Datensätzen.

__Beispiel:__
````sql
SELECT MAX(column_name) FROM table_name WHERE condition; 
````

### COUNT()
Gibt an wie viele Datensätze diesem Kriterium entsprechen.

__Beispiel:__
````sql
SELECT COUNT(column_name) FROM table_name WHERE condition; 
````

### AVG()
Gibt den Durchschnittswert einer numerischen Spalte an.

__Beispiel:__
````sql
SELECT AVG(column_name) FROM table_name WHERE condition; 
````

### SUM()
Gibt die Summe aller Werte einer numerischen Spalte an.

__Beispiel:__
````sql
SELECT SUM(column_name) FROM table_name WHERE condition;
````

### Select Top (LIMIT)
Gibt nur so viele Werte zurück wie im Limit festgelegt wurden.  
Folgendes Beispiel funktioniert mit MySQL.  
Andere Datenbanksysteme haben eine andere Syntax.  

__Beispiel:__
````sql
SELECT column_names FROM table_name WHERE condition LIMIT number;
````

## Andere Befehle

---

### Aliases
Damit kann man einer Tabelle oder Spalte einen Alias für die Abfrage geben.

__Beispiel:__
````sql
SELECT column_name AS alias1 FROM table_name WHERE alias1 == 'test';
````

### Union
Mehrere Selects zusammenfügen

__Beispiel:__
````sql
SELECT column_names FROM table1 UNION SELECT column_names2 FROM table2;
````

### Order by
Ordnet die Datensätze nach der angegebenen Spalte.
Aufsteigend: ASC.
Absteigend: DESC

__Beispiel:__
````sql
SELECT col1, col2 FROM table ORDER BY col2 ASC;
````

### Group by
Das SQL GROUP BY ist eine Funktion, die es ermöglicht,
Zeilen in einer Tabelle zu gruppieren, indem sie nach einer oder mehreren 
Spalten sortiert werden.  
Anschließend können aggregierte Funktionen wie SUM oder COUNT auf diese Gruppen 
angewendet werden, um Zusammenfassungen der Daten zu erhalten.

__Beispiel:__
````sql
SELECT COUNT(CustomerID), Country FROM Customers GROUP BY Country;
````

### Having
__Wird bei Group by verwendet!__  
SQL Having ist eine Klausel in einer SQL-Abfrage, die auf Gruppen von Daten 
angewendet wird, die durch die GROUP BY-Klausel definiert sind. 
Die HAVING-Klausel ermöglicht es, eine zusätzliche Bedingung auf 
die Ergebnisse einer Gruppenaggregatfunktion wie SUM, COUNT oder AVG anzuwenden, 
um nur die Gruppen anzuzeigen, die diese Bedingung erfüllen. 
Dadurch können Daten weiter gefiltert werden, um spezifischere Ergebnisse zu erhalten.

__Beispiel:__
````sql
SELECT COUNT(CustomerID), Country FROM Customers GROUP BY Country HAVING COUNT(CustomerID) > 5;
````

## Join

### (INNER) JOIN
Returns records that have matching values in both tables.

![Inner Join](../../../img/bfks/img_innerjoin.gif)

__Beispiel:__
````sql
SELECT col_name FROM table1 INNER JOIN table2 ON table1.col = table2.col;
````

### LEFT (OUTER) JOIN
Returns all records from the left table, and the matched records from the right table.

![left Join](../../../img/bfks/img_leftjoin.gif)

__Beispiel:__
````sql
SELECT col_name FROM table1 LEFT JOIN table2 ON table1.col = table2.col;
````

### RIGHT (OUTER) JOIN
Returns all records from the right table, and the matched records from the left table.

![right Join](../../../img/bfks/img_rightjoin.gif)

__Beispiel:__
````sql
SELECT col_name FROM table1 RIGHT JOIN table2 ON table1.col = table2.col;
````

### FULL (OUTER) JOIN
Returns all records when there is a match in either left or right table.

![right Join](../../../img/bfks/img_fulljoin.gif)

__Beispiel:__
````sql
SELECT col_name FROM table1 FULL OUTER JOIN table2 ON table1.col = table2.col;
````