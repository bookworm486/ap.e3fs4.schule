# Variablen

## Weiterführende Links

* [Variablen | Microsoft](https://learn.microsoft.com/de-de/dotnet/csharp/language-reference/language-specification/variables)
* [Variablen und Datentypen | Visual C# 2012 - das umfassende Handbuch](https://openbook.rheinwerk-verlag.de/visual_csharp_2012/1997_02_003.html#dodtpa310dc3e-560e-481e-b4c8-ceef4a3e3185)

### Videos

* [Variablen und Datentypen | Programmieren Starten](https://www.youtube.com/watch?v=f2VfGAzFLjs&list=PL_pqkvxZ6ho18awjThtUMZio-yvc79TGi&index=2)