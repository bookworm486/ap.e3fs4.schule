---
tags:
- FISI
- FIAE
---

# Scrum

![Scrum model](../../img/bfks/scrum.png)

## Rollen:

### Product Owner
- Erstellung und Priorisierung des Product Backlogs 
- Verantwortlich für die Kommunikation mit Stakeholdern
- Er ist für die Eigenschaften und den Erfolg des Produkts verantwortlich

### Scrum Master
- Arbeitet mit dem Entwicklungsteam zusammen
- Unterstützung des Teams bei Problemlösung und Verbesserung

### Entwicklungsteam
- Umsetzung von Product Backlog Items 
- Selbstorganisation und gemeinsame Entscheidungsfindung

### Stakeholder
- Personen oder Gruppen, die von dem Projekt oder Produkt betroffen sind
- Haben Interesse an dem Ergebnis des Projekts und können das Produkt beeinflussen
- Werden regelmäßig vom Product Owner informiert und einbezogen, um sicherzustellen, dass ihre Bedürfnisse und Erwartungen erfüllt werden.

## Sprint:
Ist ein festgelegter Zeitrahmen von einem bis vier Wochen, 
in dem das Scrum-Team ein potenziell auslieferbares Inkrement eines Produkts erstellt. 
Während des Sprints arbeitet das Team an Aufgaben aus dem Sprint Backlog,
um das Sprint-Ziel zu erreichen, welches zu Beginn des Sprints vom 
Product Owner definiert wurde. Am Ende des Sprints findet ein 
Sprint-Review statt, um das erreichte Inkrement zu demonstrieren 
und Feedback von Stakeholdern zu sammeln.

## Daily:
Ist ein tägliches Treffen innerhalb eines Sprints. 
Das Meeting findet in der Regel morgens statt und ist darauf ausgelegt, 
dass das Entwicklungsteam sich kurz und prägnant über den Fortschritt der Arbeit 
und Herausforderungen austauscht. Jedes Teammitglied teilt seine Fortschritte, 
seine Pläne für den Tag und eventuelle Hindernisse mit, die das Team daran hindern könnten, 
das Sprint-Ziel zu erreichen. Das Meeting soll nicht länger als 15 Minuten dauern 
und dient dazu, das Team auf dem gleichen Wissensstand zu halten und sicherzustellen, 
dass die Arbeit im Einklang mit den Sprint-Zielen bleibt.

## Backlog
Ist eine geordnete Liste von Aufgaben, Funktionen, Anforderungen und Verbesserungsideen, 
die das Produktteam umsetzen möchte, um das Produkt oder die Dienstleistung zu verbessern.  
Das Product Backlog wird vom Product Owner verwaltet und priorisiert. 
Die Einträge auf der Liste sind in der Regel User-Stories.

## Epic
Ein Sprint Epic ist eine große User-Story, die im Scrum Product Backlog enthalten 
ist und in kleinere User Stories oder Aufgaben aufgeteilt werden muss, um sie innerhalb
eines Sprints umsetzen zu können.

## Retro
Eine Sprint Retrospective ist ein Meeting, das am Ende eines Sprints stattfindet. 
Es ist ein wichtiger Bestandteil des Scrum-Frameworks, da es dem Team die Möglichkeit gibt, 
über die vergangenen Ereignisse des Sprints zu reflektieren, 
um aus Erfolgen und Fehlern zu lernen und kontinuierlich Verbesserungen vorzunehmen.

## Review
Steht am Ende des Sprints. Hier überprüft das Scrum-Team das Inkrement, 
um das Product Backlog bei Bedarf anzupassen.

## Planning:
Ist ein Meeting, das am Anfang eines Sprints stattfindet. 
Es ist ein wichtiges Element des Scrum-Frameworks, da es dem Team die Möglichkeit gibt, 
den nächsten Sprint zu planen und sicherzustellen, dass alle Beteiligten ein 
gemeinsames Verständnis der Aufgaben, Ziele und Prioritäten haben.

## Definition of Done (DoD)
Die DoD ist ein gemeinsames Verständnis im Scrum-Team darüber, was es bedeutet, 
eine Aufgabe oder ein Feature als abgeschlossen anzusehen. 
Sie beschreibt alle notwendigen Aktivitäten und Qualitätskriterien, 
um sicherzustellen, dass das Ergebnis des Sprints potenziell auslieferbar 
und kundenfähig ist.