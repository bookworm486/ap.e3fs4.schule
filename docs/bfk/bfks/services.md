---
tags:
- FISI
- FIAE
---

# Services

## Infrastructure as a Service (IaaS)
IaaS bedeutet, dass ein Anbieter eine virtuelle Infrastruktur bereitstellt, 
die der Benutzer skalieren und verwalten kann, ohne physische Hardware zu besitzen.  

### Beispiel: 
- Amazon Web Services (AWS) (Cloud Anbieter)
- Microsoft Azure
- Google Cloud Platform (GCP)
- IBM Cloud

## Software as a Service  (SaaS)
Eine Anwendung wird über das Internet bereitgestellt und von einem Anbieter verwaltet, 
was dem Benutzer ermöglicht, sie nach Bedarf zu nutzen, ohne dass er sie selbst 
installieren oder warten muss.

### Beispiel: 
- Microsoft Office 365
- Dropbox
- Zoom

## Platform as a Service (PaaS)
Ein Anbieter stellt eine Plattform bereit, auf der der Benutzer Anwendungen entwickeln, 
testen und bereitstellen kann, ohne sich um die Infrastruktur kümmern zu müssen.

### Beispiel:
- Heroku
- Google App Engine
- Firebase
