# Service Level Agreement

Unternehmen fragen mehr und mehr **IT-Dienstleistungen** von **IT-Dienstleitern** nach, um ihre Geschäftsprozesse im globalisierten Wettbewerb aufgrund von Kundenwünschen **qualitäts-** und **preisbewusst** gestalten zu können (Beispiel: Einrichtung eines Callcenters für den Onlineverkauf von Produkten und Dienstleistungen eines Unternehmens).

Die hohe **Qualität** der erforderlichen IT-Dienstleistungen ist für die Unternehmen bei wachsender Wettbewerbsintensität ein entscheidender Wettbewerbsvorteil im Ringen um die Kundschaft.
So ist beispielsweise bei einem Callcenter für den Kunden ausgesprochen wichtig, zu welchen Zeiten er das Callcenter erreichen kann und wie lange er unter Umständen in einer Wartescheife auf den Kontakt warten muss.
Die qualitativen Anforderungen an die IT-Dienstleistung werden in einem sogenannten **Service-Level-Agreement (SLA)**, einer vertraglichen Vereinbarung zwischen einem Dienstleistungsempfänger und einem IT-Dienstleistungslieferanten festgeschrieben.

Die vielfältigen qualitativen Anforderungen an die IT-Dienstleistung machen es unumgänglich, die einzelnen Wünsche des Kunden als **definierte Kundenanforderungen** exakt zu beschreiben, damit der IT-Dienstleister die entsprechenden **Preise** für seine Services kalkulieren kann.
Dabei ist es auch besonders wichtig, beispielsweise bei technischen Störungen der IT-Dienstleistung Vertragsstrafen o.Ä. von vorneherein festzulegen, um langfristige juristische Streitigkeiten von Beginn an auszuschließen.
Auch für den IT-Dienstleister ist es von großer Bedeutung, wie zufrieden der IT-Kunde mit der im SLA vereinbarten Dienstleistung ist - schließlich sprechen sich qualitativ geringwertige Leistungen im Unternehmensumfeld schnell herum und verschlechtern den Ruf des IT-Unternehmens, was im Extremfall sogar dessen Insolvenz zur Folge haben kann.
So hat ebenso wie der IT-Kunde auch der IT-Dienstleister ein hohes Interesse an einer genauen Beschreibung aller gewünschten IT-Dienstleistungen.

## Inhalte

Ein Service-Level-Agreement sollte folgende **Inhalte** aufweisen:

* genaue Nennung der beiden **Vertragspartner**
* Vertragsbeginn und Vertragsende bzw. **Vertragslaufzeit**
* exakte Beschreibung der vereinbarten **IT-Dienstleistungen** in einer **genau definierten Qualität**, dem sogenannten **Service-Level** unter Nutzung von vereinbarten **Kennzahlen**, eventuell Festlegung von vereinbarten Toleranzgrenzen bei Kennzahlen
* **Preisvereinbarung** über die festgeschriebenen IT-Dienstleistungen unter Berücksichtigung eventueller unterschiedlicher Service-Levels, Festlegung von **Zahlungsmodalitäten**
* Nennung der **Ansprechpartner** in beiden Unternehmen bei Vertragsstörungen, Beschwerden oder gewünschten Vertragsänderungen
* vereinbarte **Regelungen bei Leistungsstörungen oder Vertragsänderungen**
* **Maßnahmen zur Qualitätssicherung** der IT-Services (Einrichtung eines Service-Level-Berichtswesens, das Leistungen sowie Störungen genau dokumentiert)
* **Glossar**