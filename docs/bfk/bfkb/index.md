# Grundlagen der Betriebswirtschaftslehre

## 1. Gegenstand der Betriebswirtschaftslehre

Im Zentrum traditioneller Betriebswirtschaftslehre steht die Untersuchung unternehmerischen Handelns = der **Entscheidungsprozess** in einem **privaten Betrieb** im **marktwirtschaftlichen Wettbewerb**. Menschen haben gewisse materielle Bedürfnisse. In der Wirtschaftslehre spricht man von der Nachfrage nach Gütern und Dienstleistungen. Die nachgefragten Güter und Dienstleistungen werden von Betrieben erstellt.

### Begriffliche Abgrenzungen

* **Betrieb:** Eine planvoll organisierte Wirtschaftseinheit, in der, Produktionsfaktoren kombiniert werden, um Güter und Dienstleistungen herzustellen und abzusetzen
* **Unternehmung:** (Das große Ganze) Eine rechtlich selbständige Einheit mit wirtschaftlicher Zielsetzung. Sie besteht unter anderem aus einem oder mehreren Betrieben
* **Firma:** Name, unter dem ein Kaufmann seine Geschäfte betreibt
* **Fabrik:** Produktionsstätte; Ort der Erstellung von Sachgütern
* **Geschäft:** Ort der Abwicklung von Einzeltransaktionen

## Betriebliche Leistungserstellung

![Leistungserstellung](../../img/bfkb/leistungserstellung.png)

1. Am Beschaffungsmarkt erwirbt der Betrieb die Produktionsfaktoren:
   * Arbeit
   * Betriebsmittel (Anlagen, Fuhrpark, Betriebsgebäude, Werkzeuge)
   * Werkstoffe
     * Rohstoffe (Hauptbestandteile z.B. Stahlblech für Karosserien)
     * Hilfsstoffe (Nebenbestandteile z.B. Schrauben, Nieten, Lötzinn)
     * Betriebsstoffe (nicht unmittelbarer Bestandteil z.B. Maschinenöl)
2. Produktionsfaktoren werden in Produkte umgewandelt
3. Produkte/Dienstleistungen werden abgesetzt (an private Haushalte oder betriebliche Abnehmer)
4. Einzahlungen aus dem Absatz
5. Auszahlungen an Lieferanten der Produktionsfaktoren -> finanzielle Mittel verringern sich
6. Beschaffung finanzieller Mittel am Kapitalmarkt (z.B. Kredite oder durch Aktien usw.) für Investitionen
7. Auszahlung von Dividenden und Fremdkapitalzinsen
8. Zuschüsse und Subventionen
9. Erhobene Steuern führen beim Betrieb zu einem Mittelabfluss

Der Güterebene steht also eine Geldebene gegenüber, die man auch als **Liquidität** bezeichnet. Auf Dauer kann sich ein Betrieb nur dann am Markt behaupten, wenn seine Einzahlungen höher sind als seine Auszahlungen. Es existieren **private Betriebe** und **öffentliche Betriebe**. Private Betriebe arbeiten nach dem erwerbswirtschaftlichen Prinzip = Streben nach (maximalem) Gewinn. Öffentliche Betriebe streben – von Ausnahmen abgesehen- nicht nach Gewinn. Sie arbeiten nach dem Kostendeckungsprinzip (z.B. städt. Wasserwerk, städt. Kindergarten) und Zuschussprinzip (z.B. Museen, Kulturzentren).

## [2. Unternehmensziele](./unternehmung.md)

Die Unternehmensziele leiten sich aus dem Unternehmensleitbild ab. Sie geben der Unternehmensleitung, den Bereichs- und Gruppenleitern bzw. den Mitarbeitern eine Orientierung für die Steuerung und Kontrolle der betrieblichen Prozesse.\
**Unternehmensziele** beschreiben einen zukünftigen Zustand des Unternehmens, den der zuständige Entscheidungsträger anzustreben hat. Sie können nach einer Vielzahl von Kriterien gegliedert werden. Wir be­schränken uns im Folgenden auf die Darstellung folgender Gliederungskriterien.

###### Gliederung nach dem angestrebten Erfolg des Unternehmens

![Magisches Zieldreieck](../../img/bfkb/magischesZieldreieck.png)

Die Ziele der Unternehmen nach dem angestrebten Erfolg sind dreifacher Art:

1. **Ökonomische (wirtschaftliche) Ziele:** Es gilt, einen möglichst großen Überschuss an Erfolg über den Mitteleinsatz zu erlangen.
   * *Marktziele:* Sie definieren die Stellung, die das Unternehmen im Markt einzunehmen anstrebt. Hierzu zählen z.B. das Streben nach Marktanteilsvergrößerung, das Erreichen bestimmter Wachstumsziele, das Streben nach Prestige und Macht oder das Streben nach Unabhängigkeit.
   * *Ertragsziele:* Es handelt sich hier um Ziele, die sich in Ziffern ausdrücken lassen, wie z.B. die Höhe des angestrebten Gewinns, die Rentabilität des eingesetzten Kapitals oder die geplante Umsatzentwicklung.
   * *Leistungsziele:* Diese Zielvorgaben charakterisieren die angestrebten Leistungs­schwerpunkte des Unternehmens. Hierzu können z.B. gerechnet werden: das Streben nach einem hohen Qualitätsstandard durch ein Qualitätsmanagement.
2. **Ökologische Ziele:** Das Umweltschonungsprinzip hat die ökologischen Interessen zu berücksichtigen; Umweltbelastungen sind so gering wie möglich zu halten.
3. **Soziale Ziele:** Neben wirtschaftlichen und ökologischen Zielen verfolgen die Unternehmen auch soziale Ziele. Soziale Ziele stellen den Menschen in den Mittelpunkt des Leistungsprozesses; seinen Erfordernissen ist gleichermaßen Rechnung zu tragen. Beißpiele:
   * Wirtschaftliche Besserstellung der Arbeitnehmer
   * Ausgleich familiärer Belastungsunterschiede
   * Altersabsicherung und Absicherung gegen Risiken des Lebens
   * Förderung geistiger und sportlicher Interessen Die Verfolgung sozialer Ziele wird den Arbeitgebern aber auch gesetzlich vorgeschrieben, insbesondere durch das Arbeitsschutzrecht.

#### Zielharmonie und Zielkonflikt

###### Zielkonflikt:

Die Verfolgung eines Ziels(ökonomisch/ökologisch/sozial) beeinträchtigt oder verhindert die Erreichung eines anderen Ziels. Beispiele:

* Ökonomie und soziale Ziele: hohe Löhne und Gehälter stehen in Konflikt mit der Gewinnmaximierung (Personalkosteneinsparung)
* Ökologie und Ökonomie: Umweltverschmutzung verringern aber gleichzeitig mehr produzieren.

###### Zielharmonie:

Die Förderung eines Ziels (ökonomisch/ökologisch/sozial) begünstigt zugleich die Förderung eines oder mehrerer anderer Ziele. Beispiele:

* Weniger Kosten der Materialien durch Verwendung recyclebarer Materialien
* Ökologisch und ökonomisch: Guter Ruf führt zu höherem Absatz

## 3. Wirtschaften

Unter Wirtschaften werden alle Tätigkeiten verstanden, die bewusst der Bedürfnisbefriedigung durch Güter dienen, unter der Rahmenbedingung der Knappheit von Gütern

#### Wirtschaftliches (ökonomisches) Prinzip:

Wirtschaftsgüter sind knapp. Darum bemühen sich Menschen, sie sparsam und vernünftig einzusetzen (Rational- oder Vernunftprinzip).

* **Maximalprinzip (Haushaltprinzip):** Es verlangt, dass mit gegebenen Mitteln eine möglichst hohe Leistung erzielt wird.
* **Minimalprinzip (Sparprinzip):** Es verlangt, dass eine vorbestimmte Leistung mit möglichst geringen Mitteln erzielt wird.
