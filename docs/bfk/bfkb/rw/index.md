# Rechnungswesen

Betriebliches Rechnungswesen als **Tätigkeit** ist das systematische zahlenmäßige Erfassen, Aufbereiten, Analysieren, Auswerten und Darstellen von Zahlen als Mengen und Wertgrößen aller wirtschaftlichen Tatbestände eines Betriebs und seiner Beziehung zu anderen Betrieben

## Aufgaben

- Dokumentationsfunktion (Beweissicherung)  
- Rechnungsmaßlegung für Kapitalgeber, Finazamt, Banken  
- Kontrollfunktion (Überwachung von Einnahmen und Ausgaben)  
- Planungsfunktion für unternehmerische Entscheidungen  
- Informationsfunktion  

## Bereiche

- Finanzbuchhaltung:  
	- **Externes Rechnungswesen**  
	- Jahresabschluss (u. a. Bilanz, GuV)  
	- Erfassung von Geschäftsvorfällen  
	- Übersicht über Vermögen, Konten und Schulden  
	- Finanzbuchhaltung = Grundlage für Finanzamt, Bank, Investitionen  
	- Erfolg des Unternehmens: Gewinn oder Verlust?  
- Kosten- und Leistungsrechnung:  
	- **Internes Rechnungswesen**  
	- Kalkulationsgrundlage zur Preisgestaltung  
	- Kosten werden Kostenstellen zugeordnet  
	- Erfolg einer Abteilung wird ermittelt  
- Statistik:  
	- z.B. Umsatzstatistik  
		- Vergleich mit Vorjahr  
		- mit anderen Unternehmen  
	- Budgetplanung  
	- Investitionsplanung  
	- Grundlage der unternehmerischen Planung  
	- Grundlage für dem Vergleich gegenüber anderen Unternehmen in der Branche  
- Planungsrechnung:  
	- **Betriebliches Rechnungswesen**  
	- Schätzung zu zukünftigen Einnahmen/Ausgaben (Umsatzerwartungen)  
	- Festlegen von Richtwerten  
	- Basis für den Jahresplan  
	- Vergleiche zu Vorjahren um Schlüsse daraus zu ziehen