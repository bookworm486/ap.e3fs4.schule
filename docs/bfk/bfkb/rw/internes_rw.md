# Internes Rechnungswesen

## Kosten- und Leistungsrechnung

### Übersicht

![Übersicht Kostenrechnung](../../../img/bfkb/uebersicht_kostenrechnung.png)

![Systemzusammenhang Kostenrechnung](../../../img/bfkb/systemzusammenhangKostenrechnung.png)

## Vollkostenrechnung

* alle Kosten werden auf Erzeugnisse umgelegt
* Einzelkosten: 
    * können dem Produkt direkt zugeordnet werden (Material; Fertigungslöhne)
* Gemeinkosten: 
    * können dem Produkt nicht direkt zugeordnet werden (Miete, Verwaltung, Vertrieb)

## Teilkostenrechnung

* nur ein Teil der Kosten wird auf das Erzeugnis umgelegt
* fixe Kosten: Mengenunabhängig (Miete, Gehälter)
* variable Kosten: Mengenhabhängig (Rohstoffe)

## Deckungsbeitragrechnung

= Erlös - variable Kosten

* Deckungsbeitrag ist der Betrag, der zur Verfügung steht zur Deckung der fixen Kosten
* Ziel Deckungsbeitrag größer 0 oder gleich 0
