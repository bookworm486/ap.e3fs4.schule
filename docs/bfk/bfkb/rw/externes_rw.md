# Externes Rechnungswesen

## Bilanz

### Aufbau einer Bilanz

![Aufbau einer Bilanz](../../../img/bfkb/aufbauBilanz.png)

- Bilanz ist die kurzgefasste Darstellung von Vermögen, Schulden, Eigenkapital  
- Linke Bilanzseite:  
	- Gliederungsprinzip: zunehmende Flüssigkeit (Liquidität)  
	- Kapitalverwendung = Investition (Wofür?)  
- Rechte Bilanzseite:  
	- Gliederungsprinzip: zunehmende Fälligkeit  
	- Kapitalherkunft = Finanzierung (Woher?)

## Gewinn und Verlustrechnung (GuV)

* bildet mit der Bilanz den Jahresabschluss
* Ziel ist es den Erfolg über eine Periode zu bestimmen und gewinnbringende Bereiche zu erkennen

-> Aufwände werden Erträgen gegenübergestellt

![Aufbau einer GuV](../../../img/bfkb/guv.jpg)