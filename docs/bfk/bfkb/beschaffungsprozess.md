# Beschaffungsprozess

![Der Beschaffungsprozess im Einkauf](../../img/bfkb/beschaffungsprozess.jpg)

## 1. Bedarfsermittlung

Am Anfang jedes Bestellprozesses steht die Bedarfsermittlung.
Die Bedarfsermittlung verschafft Überblick darüber, welche Güter und Leistungen im Unternehmen benötigt werden.
Dafür wird der Materialbestand mit den Produktions- und Verkaufsaufträgen sowie den Einkaufsbestellungen abgeglichen.

Generell unterscheidet man bei der Bedarfsermittlung nach drei Bedarfsarten:  
Primärbedarf (z. B. verkaufsfähige Baugruppen und Ersatzteile), Sekundärbedarf (z. B. Rohstoffe und Einzelteile) sowie Tertiärbedarf (z. B. Hilfsstoffe und Betriebsstoffe).

Grundsätzlich gibt es drei Methoden zur Bedarfsermittlung:  
die deterministische, die stochastische und die heuristische Bedarfsermittlung.
Bei der deterministischen Methode wird der Bedarf anhand von Stücklisten und Rezepturen ermittelt.
Bei der stochastischen Methode greift man auf die Verbrauchswerte der Vergangenheit zurück.
Hierfür werden mathematische Verfahren wie z. B. Regressionsanalysen, arithmetische Mittelwerte und dergleichen verwendet.
Bei der heuristischen Methode wird der Bedarf anhand einer subjektiven Schätzung erfahrener Mitarbeiter ermittelt.

**Checkliste:**

* Wurde der korrekte Mengen- und Qualitätsbedarf ermittelt?
* Wie viel Material befindet sich durchschnittlich im Lager?
* Wie oft schlägt sich das Lager jährlich um?
* Wie lange lagern einzelne Materialien durchschnittlich im Lager?
* Welche Güter und Leistungen müssen zugekauft werden?

## 2. Bestandskontrolle

Die Bestandskontrolle ist ein wichtiger Teil der Bestandsermittlung.
Sie liefert den Vergleich von Sollwert und Istwert von Beständen in Form von Artikelstammdaten, Seriennummern und Preislisten.
Daraus werden wichtige Kennzahlen gewonnen über aktuelle Lagerbestände, Mindestbestände, Durchschnittsverbrauche, Einkaufspreise, geplante Bestellungen und vieles mehr.
Die Bestandskontrolle ist eine wichtige Voraussetzung für die Vorbereitung der Bestellung.

**Checkliste:**

* Sind alle Materialbestände, -bewegungen, -eingänge und –entnahmen dokumentiert?
* Wie steht es um Verweildauer und Umschlagsrate?
* Wann wird wie viel bestellt?
* Wo werden Posten in Ihrem Unternehmen gelagert?
* Welche Posten müssen wann gezählt werden?

## 3. Budgetfreigabe

Nachdem Bestand und Bedarf geklärt wurden, gilt es, das Budget zu klären.
Dies ist grundsätzlich Aufgabe des Controllings und nicht der Materialwirtschaft.
Generell werden vor einer Budgetfreigabe alle geplanten Bestellungen durch eine (meist interne) Kostenstelle auf die Einhaltung eines Gesamtfinanzrahmens geprüft.
In diesem Zusammenhang werden auch Liquiditätssicherungsmaßnahmen durchgeführt, sodass viele Bestellungen „maßgeschneiderte“ Transaktionen darstellen.

## 4. Lieferantenauswahl

Ist das Budget freigegeben, ist es an der Zeit, die geeigneten Lieferanten auszuwählen.
Auch hier gilt es, äußerst sorgfältig vorzugehen, denn die Qualität Ihrer Dienstleistung oder Ihrer Produkte hängt in hohem Maße von der Qualität der zugekauften Materialien, Produkte und Dienstleistungen ab.
Aus diesem Grund ist die Lieferantenauswahl auch als Teil der strategischen Beschaffung zu betrachten und erfolgt in der Regel eher langfristig im Vorfeld.

Für die Überprüfung von (neuen) Lieferanten bietet es sich an, eine formale Befragung in Form eines Fragebogens durchzuführen.
Auch können externe Audits beim Lieferanten durchgeführt werden, um sich von dessen Qualitätssystem vor Ort zu überzeugen.
Und zu guter Letzt sollten Muster eingehend geprüft werden.

**Checkliste:**

* Ist der Lieferant geeignet, Ihre vorgegebenen Qualitätsanforderungen zu erfüllen?
* Wie ist die Preis- und Kostenentwicklung des Lieferanten?
* Wie steht es um Liefertermine und Termintreue?
* Wie sind die Lieferbedingungen?
* Wie lang sind die Lieferzeiten?
* Kann man Muster anfordern?
* Gibt es einen festen Ansprechpartner?
* Ist der Lieferant flexibel und kann er just-in-time reagieren?
* Was für Zahlungsfristen gibt es?
* Welche Zahlungsarten stehen zur Verfügung?
* Welche Rabatte können gewährt werden?
* Ist die Vereinbarung eines Geheimhaltungsvertrages vonnöten?

## 5. Bestellung

Mit der Lieferantenauswahl ist der Entscheidungsprozess abgeschlossen.
Dabei ist es möglich, bei der Auswahl auch mehrere Lieferanten zu berücksichtigen, um gegebenenfalls zu einem Alternativanbieter wechseln zu können.
Nun folgt die Bestellung.
Die Bestellung ist die formale Aufforderung an einen Lieferanten, ein Produkt oder eine Dienstleistung bereitzustellen.
Erfolgt die Bestellung unmittelbar auf ein verbindliches Angebot eines Lieferanten und innerhalb der Angebotsbindefrist, kommt ein Vertrag direkt zustande.
War das Angebot freibleibend, so muss der Lieferant zunächst mit einer Bestellbestätigung auf die Bestellung antworten.

**Checkliste:**

* Enthält die Bestellung alle Artikel, Menge und Mengeneinheit?
* Sind Liefertermin und Lieferadresse genannt?
* Enthält die Bestellung alle Informationen zu Preis, Währung und Zahlungsbedingungen?
* Ist die Lieferantenadresse korrekt?
* Wurde eine Bestellhistorie angelegt?
* Sind alle Rückgabe-Möglichkeiten und -Modalitäten geklärt?
* Gibt es Möglichkeiten zur Sendungsverfolgung?
* Gelten gegebenenfalls besondere Garantiebedingungen oder länderspezifische Besonderheiten?
* Ist der Kundenservice des Lieferanten jederzeit gut zu erreichen?

## 6. Bestellüberwachung

Da es vorkommen kann, dass ein Lieferant Lieferfristen nicht einhält oder falsche Artikel liefert, ist eine Bestellüberwachung unumgänglich.
Hierfür ist es wichtig, die Bestellhistorie genau zu dokumentieren.
Nur eine korrekt geführte Bestellhistorie ermöglicht es, rechtzeitig auf Überraschungen zu reagieren (z. B. in Form von Mahnungen), und so Nachteile für den eigenen Wertschöpfungsprozess abzuwenden.
Für die Bestellüberwachung bieten sich Systeme an, die in die betriebseigene IT eingebunden werden.

**Checkliste:**

* Ist die Bestellhistorie klar und aussagekräftig?
* Werden Termine eingehalten oder verschiebt sich etwas?
* Ist alles für ein eventuelles Mahn- und Erinnerungswesen vorbereitet? 
* Müssen Bestellpositionen storniert werden?

## 7. Wareneingang

Der Wareneingang umfasst mehr als nur die physische Annahme der von Ihnen getätigten Bestellung(en).
Hierzu zählen außerdem die Dokumentation des Erhalts sowie das Weiterleiten der Objekte und deren Eingangsdaten.
Auch muss der Wareneingang in Wert und Menge in der Buchhaltung erfasst werden.
In der Regel erfolgen diese Buchungen mithilfe der Lieferscheine oder Wareneingangsrechnungen.

Beim Wareneingang ist es wichtig, dass die gelieferten Waren anhand des Lieferscheins genau überprüft werden.
Sollten beim Wareneingang Mängel festgestellt werden, muss die Annahme verweigert, bzw. die Ware umgehend beim Lieferanten reklamiert werden.
Geschieht dies nicht, gilt die auf dem Lieferschein stehende Menge als geliefert und schadensfrei.

**Checkliste:**

* **Vor der Warenannahme**
    * Ist die Ware für Sie bestimmt?
    * Liegt die Bestellung für die Ware tatsächlich vor?
    * Stimmt die Anzahl der gelieferten Packstücke mit der Anzahl auf den Lieferpapieren überein?
    * Sind tauschpflichtige Artikel in einem tauschfähigen Zustand?
    * Sind äußerlich erkennbare Schäden an der Ware vorhanden?
    * Wurde der Liefertermin eingehalten?
* **Nach der Warenannahme**
    * Enthalten die Packstücke das, was außen draufsteht?
    * Weist die Ware Schäden auf, die von außen nicht sichtbar waren?

## 8. Zahlungsabwicklung

Ist die Ware angenommen, überprüft die Buchhaltung die Rechnung, sobald diese vorliegt.
Die Rechnung sollte sowohl mit der Bestellung als auch mit dem Lieferschein übereinstimmen.
Ist dies nicht der Fall, ist es Aufgabe der Buchhaltung, diese Diskrepanzen zu beseitigen.
Hierfür nimmt die Buchhaltung sowohl mit dem Lieferanten als auch mit dem Einkauf Kontakt auf.
Ist alles geklärt, wird die Rechnung genehmigt und gebucht.
Es wird eine Zahlungsermächtigung erstellt und der Rechnungsbetrag kann über die Bank beglichen werden.

**Checkliste:**

* Stimmen Bestellung und Daten auf dem Lieferschein überein?
* Stimmen alle Bankdaten?
* Sind alle Zahlschritte überprüft und genehmigt worden?
* Ist klar, wo und wie die Rechnung abgelegt werden muss?

Mit der Bezahlung der Rechnung ist der Beschaffungsprozess abgeschlossen.