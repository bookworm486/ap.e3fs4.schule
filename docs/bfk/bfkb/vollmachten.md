# Vollmachten

Stellvertretung ist das Handeln im Namen eines anderen.
Die betriebliche Vollmacht (Vertretungsmacht) ist das Recht, im Namen und für die Rechnung des Betriebes verbindliche Willenserklärungen abzugeben.
Also Rechtsgeschäfte schließen, ändern und auflösen zu können.  
Handelt ein Vertreter im Rahmen seiner Vollmacht, so ergeben sich daraus direkte Rechtsfolgen für das Unternehmen (§ 164 I BGB).
Überschreitet der Angestellte seine Vollmacht oder handelt er gar ohne Vollmacht bzw. Vertretungsmacht, ist er dem anderen Vertragspartner nach dessen Wahl zur Vertragserfüllung oder zum Schadensersatz verpflichtet (§ 179 I BGB).

Das Handelsrecht als Sonderrecht für Kaufleute unterscheidet handelsrechtliche Vollmachten:

* **Prokura:** Ist die am weitesten reichende Vollmacht und berechtigt zu allen Arten von Rechtshandlungen nach innen und nach außen, „die der Betrieb eines Handelsgewerbes mit sich bringt“ (vgl. §49 HGB)
* **Handlungsvollmacht:** Ist im Gegensatz zur Prokura eine eingeschränkte Vollmacht. Sie kann sehr weit gefasst oder auf einzelne Geschäfte beschränkt sein.