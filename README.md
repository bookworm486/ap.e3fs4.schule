# Wiki für AP2

This repo is used as a wiki for the final exams of the so called german Ausbildung for system administrators.
It will be only in german so far!!!
The wiki is hosted on [https://ap.e3fs4.schule/](https://ap.e3fs4.schule/).

## Install it yourself

The installation guide is only for Linux. If you want to host it on another platform refer to the different projects used for this wiki (mainly [https://www.mkdocs.org/](https://www.mkdocs.org/)).

1. Install dependcies:  
    1. mkdocs: `pip install mkdocs`
    2. mkdocs-material theme : `pip install mkdocs-material`
    3. mkdocs-rss-plugin: `pip install mkdocs-rss-plugin`
2. Clone the repo:

   ``` bash
   git clone https://git.project.wern3r.de/e3fs4/ap.git
   ```
3. Go into the repo:

   ``` bash
   cd  ap
   ```
4. For testing you can run:

   ``` bash
   mkdocs serve
   ```
   Now you can open the page on [http://127.0.0.1:8000/](http://127.0.0.1:8000/)

   If you want to build it instead use:

   ``` bash
   mkdocs build
   ```
   Here you get the static files in `site/`

